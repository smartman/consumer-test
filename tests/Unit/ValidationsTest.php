<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Libraries\TransactionValidator;

class ValidationsTest extends TestCase {

    use DatabaseTransactions;

    public function testTransactionIdExists() {
        $transaction = factory(\App\Transaction::class)->create();
        $validator = new TransactionValidator();

        $this->assertFalse($validator->validateTransactionId($transaction->transaction_id));
    }

    public function testTransactionIdNotExists() {
        $validator = new TransactionValidator();
        $this->assertTrue($validator->validateTransactionId(1));
    }

    public function testIsInteger() {
        $validator = new TransactionValidator();
        $this->assertTrue($validator->validateInteger(3));
    }
    
    public function testIsNegative() {
        $validator = new TransactionValidator();
        $this->assertFalse($validator->validateInteger(-3));
    }
    
    public function testHasFractions() {
        $validator = new TransactionValidator();
        $this->assertFalse($validator->validateInteger(3.3));
    }

}
