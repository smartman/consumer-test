## About this application

Easiest way to run this application is following these instructions

- Setup database
- Run `php artisan migrate`
- Run `php artisan db:seed` to create user with id=3000 
- Check for laravel log to see calculated signature and Authorization headers for each API call. Uncomment if statement in TransactionSignatureMiddleware to turn on signature comparison with Authorization
- Execute phpunit for running the unit test suite
- Send the API calls, see validation errors and successful results