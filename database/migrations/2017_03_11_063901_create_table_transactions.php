<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransactions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create("transactions", function ($t) {
            $t->engine="InnoDB";
            $t->increments("id");
            $t->integer("transaction_id")->unique();
            $t->enum("type", ["bet", "win"]);
            $t->integer("user_id")->unsigned();
            $t->foreign("user_id")->references("user_id")->on("users");
            $t->integer("round_id");
            $t->integer("session_id");
            $t->integer("amount");
            $t->integer("game_id");
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop("transactions");
    }

}
