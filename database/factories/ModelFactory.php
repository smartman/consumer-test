<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        'user_id' => 3001,
        'amount' => 1000
    ];
});

$factory->define(App\Transaction::class, function (Faker\Generator $faker) {

    return [
        "user_id" => function() {
            return factory(\App\User::class)->create()->user_id;
        },
        "transaction_id" => $faker->numberBetween(1),
        "round_id" => $faker->numberBetween(1),
        "session_id" => $faker->numberBetween(1),
        "round_id" => $faker->numberBetween(1),
        "game_id" => $faker->numberBetween(1),
        "amount" => $faker->numberBetween(1),
    ];
});
