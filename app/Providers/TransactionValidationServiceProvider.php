<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TransactionValidationServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('transactionValidator', function () {
            return new \App\Libraries\TransactionValidator();
        });
    }

}
