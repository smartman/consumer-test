<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Log;
use Closure;

class TransactionSignatureMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $url = $request->url();
        $method = $request->method();
        $date = $request->header("date");
        $bodyHash = md5($request->getContent());
        $signatureString = $url . $method . $date . $bodyHash;
        $signature = md5($signatureString);

        $authorization = $request->header("Authorization");
        info("Transactions API call signature=$signature vs Authorization=$authorization");
//        if ($authorization !== $signature) {
//            $message = "Authorization does not match signature";
//            Log::error($message);
//            return response()->json(["status" => "error", "message" => $message], 401);
//        }

        return $next($request);
    }

}
