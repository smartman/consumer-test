<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TransactionValidator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Transaction;

class TransactionConsumerController extends Controller {

    public function postTransaction(Request $request, $provider, Transaction $transaction) {
        info("Starting to process transaction for provider $provider");
        $data = json_decode($request->getContent());
        DB::beginTransaction();

        //If this validation passes then further validation in the middle is not needed for input data errors
        $validated = TransactionValidator::validateRequest($data);
        if ($validated["status"] !== "OK") {
            Log::error($validated["message"]);
            return response()->json(["status" => "error", "message" => $validated["message"]], 401);
        }

        //updates database
        $user = User::find($data->userId);
        $transaction->populateData($data);
        $transaction->save();
        $user->updateBalance($transaction);
        $user->save();
        info("Transaction $transaction->id($transaction->transaction_id) processed for user $user->user_id - $transaction->type ($transaction->amount) ");
        DB::commit();

        //prepare and send response
        return response()
                ->json($this->prepareResponse($transaction))
                ->header("date", $transaction->created_at->toAtomString());
    }

    private function prepareResponse($transaction) {
        $data = [
            "transactionReference" => $transaction->id,
            "wallet" => $transaction->user->amount,
            "transactionId" => $transaction->transaction_id,
            "gameId" => $transaction->game_id,
            "transactionType" => $transaction->type,
            "transactionAmount" => $transaction->amount,
            "userId" => $transaction->user_id,
            "references" => [[
            "referenceType" => "Round",
            "referenceId" => $transaction->round_id
                ], [
                    "referenceType" => "Session",
                    "referenceId" => $transaction->session_id
                ]],
        ];
        return $data;
    }

}
