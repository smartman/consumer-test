<?php

namespace App\Libraries;

use App\Transaction;
use App\User;

/**
 * Description of TransactionValidator
 *
 * @author smartman
 */
class TransactionValidator {

    //Validates all input values, needs validation to be processed in order
    public function validateRequest($data) {
        if (!$this->validateTransactionId($data->transactionId)) {
            return ["status" => "error", "message" => "Transaction ID not unique"];
        }
        if (!$this->validateUserId($data->userId)) {
            return ["status" => "error", "message" => "Invalid user ID. This demo app has user with ID 3000 id db:seed has been done"];
        }
        if (!$this->validateTransactionType($data->transactionType)) {
            return ["status" => "error", "message" => "Transaction type invalid"];
        }
        if (!$this->validateInteger($data->gameId)) {
            return ["status" => "error", "message" => "Game ID invalid"];
        }
        
        //Currently 0 bet or win is not allowed
        if (!$this->validateInteger($data->transactionAmount)) {
            return ["status" => "error", "message" => "Transaction amount invalid"];
        }
        if (!$this->validateReferences($data->references)) {
            return ["status" => "error", "message" => "Game Round or Session data invalid"];
        }
        if (!$this->validateHasMoney($data->userId, $data->transactionAmount, $data->transactionType)) {
            return ["status" => "error", "message" => "Please deposit more money"];
        }

        return ["status" => "OK"];
    }

    //Returns true if validation has passed
    public function validateTransactionId($transactionId) {
        $transaction = Transaction::where('transaction_id', $transactionId)->first();
        return $transaction === null;
    }

    public function validateUserId($userId) {
        $user = User::find($userId);
        return $user !== null;
    }

    public function validateInteger($int) {
        return is_int($int) && $int > 0;
    }

    public function validateTransactionType($transactionType) {
        return ($transactionType === "bet") || ($transactionType === "win");
    }

    public function validateReferences($refs) {
        foreach ($refs as $ref) {
            if ($ref->referenceType === "Round") {
                return $this->validateInteger($ref->referenceId);
            } else if ($ref->referenceType === "Session") {
                return $this->validateInteger($ref->referenceId);
            } else {
                return FALSE; //unknown referenceType
            }
        }
    }

    public function validateHasMoney($userId, $transactionAmount, $transactionType) {
        $user = User::find($userId);
        if ($transactionType === "bet" && $user->amount >= $transactionAmount) {
            return TRUE;
        } else if ($transactionType == "win") {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
