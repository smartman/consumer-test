<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
    
    public function user() {
        return $this->belongsTo("App\User", "user_id");
    }

    //Fills in new transaction data for saving
    public function populateData($data) {
        $this->user_id = $data->userId;
        $this->transaction_id = $data->transactionId;
        $this->type = $data->transactionType;
        $this->round_id = $this->getReferenceType($data->references, "Round");
        $this->session_id = $this->getReferenceType($data->references, "Session");
        $this->amount = $data->transactionAmount;
        $this->game_id = $data->gameId;
    }

    //Helper function to access Round and Session arrays easier
    private function getReferenceType($refs, $refType) {
        foreach ($refs as $ref) {
            if ($ref->referenceType === $refType) {
                return $ref->referenceId;
            }
        }
    }

}
