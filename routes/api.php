<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api','transaction_signature'])->post('/providers/{provider}/transaction', "TransactionConsumerController@postTransaction");
Route::middleware(['api','transaction_signature'])->get('/providers/{provider}/transaction', "TransactionConsumerController@postTransaction");
